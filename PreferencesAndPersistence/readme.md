![keikis Logo](media/keikis-academy-logo-small.png)

# Android PreferencesAndPersistence

Shows the principal ways to store information in the device: Internal App File, Preferences, Cache, External SD & DataBase.
Also Utils for remove and check files.
Remember full Explanations and Examples could be located on [academy.keikis.org - android-data-persistence](http://academy.keikis.org/android-data-persistence/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Android Studio 2.3


### Installing

Easy!. Download and open the project from File> Open...


### Building

Easy Too!. Build > Make Project


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **keikis academy** [academy.keikis.org](http://academy.keikis.org)


## License

This project is licensed under [Creative Commons Attribution 4.0 Unported License](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)
