/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.persistence.dbadapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import static com.keikis.persistence.dbadapter.DataBaseHelper.SQLITE_TABLE_USER;

public class UserDBAdapter {

    private DataBaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    private final Context mCtx;

    public UserDBAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public UserDBAdapter open() throws SQLException {
        mDbHelper = new DataBaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long createUser(String name, int age, String pass,
                           String role, String des) {

        ContentValues initialValues = new ContentValues();
        initialValues.put(DataBaseHelper.USER_KEY_NAME, name);
        initialValues.put(DataBaseHelper.USER_KEY_AGE, age);
        initialValues.put(DataBaseHelper.USER_KEY_PASS, pass);
        initialValues.put(DataBaseHelper.USER_KEY_ROLE, role);
        initialValues.put(DataBaseHelper.USER_KEY_DESCRIPTION, des);

        return mDb.insert(SQLITE_TABLE_USER, null, initialValues);
    }

    public boolean deleteAllUsers() {

        int doneDelete = 0;
        doneDelete = mDb.delete(SQLITE_TABLE_USER, null, null);
        return doneDelete > 0;
    }

    public ContentValues fetch(String name) {
        ContentValues row = new ContentValues();
        Cursor cur = mDb.rawQuery("SELECT name, age, role, description, pass FROM user WHERE name = ?", new String[]{String.valueOf(name)});

        //Move the cursor to first position if empty return false and don't populate the ContentValues Object
        if (cur.moveToNext()) {
            row.put("name", cur.getString(cur.getColumnIndex("name")));
            row.put("age", cur.getInt(cur.getColumnIndex("age")));
            row.put("pass", cur.getString(cur.getColumnIndex("pass")));
            row.put("role", cur.getString(cur.getColumnIndex("role")));
            row.put("description", cur.getString(cur.getColumnIndex("description")));
        }
        cur.close();
        return row;
    }

    public Cursor fetchAllUsers(){
        Cursor cur = mDb.rawQuery("SELECT _id, name, age, pass, role, description FROM User;",null);
        //move cursor to first position, if empty return null
        if (cur != null) {
            cur.moveToFirst();
        }
        return cur;
    }

    public void insertSomeUsers() {

        createUser("David", 18, "passdavid", "Admin", "Person");
        createUser("Jgc", 44, "passjgc", "Admin", "Person");
        createUser("Angeles", 17, "passangeles","User", "Person");

    }


}