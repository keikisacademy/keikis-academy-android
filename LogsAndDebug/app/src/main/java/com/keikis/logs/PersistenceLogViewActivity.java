/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.logs;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.keikis.logs.dbadapter.DataBaseHelper;
import com.keikis.logs.dbadapter.UserDBAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class PersistenceLogViewActivity extends AppCompatActivity {

    private static final String TAG = "PersistenceViewActivity";
    private static final String PREFS_NAME = "Preferences";
    private static final String PREFS_FOLDER_FILE =  "/data/data/com.keikis.logs/shared_prefs/" + PREFS_NAME + ".xml";
    private static final String FILE_NAME = "Internal.txt";

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;
    EditText editTextUserName;
    EditText editTextUserAge;
    TextView textInfoState;



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.keikis.logs.R.layout.demo);

        Log.d(TAG , "onCreate Called");

        // Get intent extra data with the layout to load
        Intent intent = getIntent();
        int layout = intent.getIntExtra(MainViewTypesActivity.VIEW_TO_LOAD, 0);

        // Create a FrameLayout in run-time and clean views
        FrameLayout frame = (FrameLayout) findViewById(com.keikis.logs.R.id.main_frame);
        frame.removeAllViews();

        // Instantiate the Inflater and load the layout from layout, load Create a FrameLayout in run-time and clean views
        LayoutInflater inflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        frame.addView(inflator.inflate(layout, null));

        // Get views from xml layout
        editTextUserName = (EditText) findViewById(com.keikis.logs.R.id.editTextUserName);
        editTextUserAge = (EditText) findViewById(com.keikis.logs.R.id.editTextUserAge);
        textInfoState = (TextView) findViewById(com.keikis.logs.R.id.textInfoState);

        Toast.makeText(this, "Scroll down to view all persistence options...",
                Toast.LENGTH_LONG).show();

    }

    public void persistenceOnClick(View v) {

        //Validate Input Data
        if (isSavingAction(v) && !validateData(editTextUserName.getText().toString(), editTextUserAge.getText().toString())) {
            showErrorMsg("Validation Error: UserName & UserAge should be not empty and UserAge should be a Number. Try Again!");
            Log.e(TAG , "persistenceOnClick:  Validation Error: UserName & UserAge should be not empty and UserAge should be a Number. Try Again!");
            return;  //No execute the persistence
        }
        else if(isLoadActionFromDB(v) && !validateData(editTextUserName.getText().toString(), "0"))
        {
            showErrorMsg("Validation Error: UserName is necessary to load a user from DB. Try Again!");
            Log.e(TAG , "persistenceOnClick:  Validation Error: UserName is necessary to load a user from DB. Try Again!");
            return;  //No execute the persistence
        }

        Log.d(TAG , "persistenceOnClick: Clearing textInfoState");
        textInfoState.clearAnimation();


        switch (v.getId()) {
            case com.keikis.logs.R.id.savePreferencesButton:
                textInfoState.setText("Saving Preferences (" + PREFS_FOLDER_FILE + ") for " + editTextUserName.getText());
                textInfoState.setTextColor(Color.YELLOW);
                Log.i(TAG , "persistenceOnClick: Saving Preferences (" + PREFS_FOLDER_FILE + ") for " + editTextUserName.getText());
                savePreferences();
                break;

            case com.keikis.logs.R.id.loadPreferencesButton:
                textInfoState.setText("Loading Preferences " + PREFS_FOLDER_FILE + ") for " + editTextUserName.getText());
                textInfoState.setTextColor(Color.BLUE);
                Log.i(TAG , "persistenceOnClick: Loading Preferences " + PREFS_FOLDER_FILE + ") for " + editTextUserName.getText());
                loadPreferences();
                break;

            case com.keikis.logs.R.id.saveInternalStorageButton:
                textInfoState.setText("Saving on Internal App (" + this.getApplicationContext().getFilesDir() + "/" + FILE_NAME + ") for " + editTextUserName.getText());
                textInfoState.setTextColor(Color.YELLOW);
                Log.i(TAG , "persistenceOnClick: Saving on Internal App (" + this.getApplicationContext().getFilesDir() + "/" + FILE_NAME + ") for " + editTextUserName.getText());
                saveInternal();
                break;

            case com.keikis.logs.R.id.loadInternalStorageButton:
                textInfoState.setText("Loading from Internal App Storage (" + this.getApplicationContext().getFilesDir() + "/" + FILE_NAME + ") for " + editTextUserName.getText());
                textInfoState.setTextColor(Color.BLUE);
                Log.i(TAG , "persistenceOnClick: Loading from Internal App Storage (" + this.getApplicationContext().getFilesDir() + "/" + FILE_NAME + ") for " + editTextUserName.getText());
                loadInternal();
                break;


            case com.keikis.logs.R.id.saveExternalStorageButton:

                textInfoState.setText("Saving on External Storage App (" + Environment.getExternalStorageDirectory() + "/" + FILE_NAME + ") for " + editTextUserName.getText());
                textInfoState.setTextColor(Color.YELLOW);

                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,  Manifest.permission.WRITE_EXTERNAL_STORAGE))
                    {
                            Log.d(TAG , "persistenceOnClick: shouldShowRequestPermissionRationale to access External Storage");

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.
                        Log.d(TAG , "persistenceOnClick: not shouldShowRequestPermissionRationale necessary Requesting Permissions  to access External Storage");

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                        // MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
                else
                {

                    Log.d(TAG , "persistenceOnClick: Permissions  to access External Storage already Granted");
                    saveExternal();
                }

                break;


            case com.keikis.logs.R.id.loadExternalStorageButton:


                textInfoState.setText("Loading on External Storage App (" + Environment.getExternalStorageDirectory() + "/" + FILE_NAME + ") for " + editTextUserName.getText());
                textInfoState.setTextColor(Color.YELLOW);

                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_CONTACTS},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                }
                else
                {

                        loadExternal();
                }


                    break;


            case com.keikis.logs.R.id.saveCacheStorageButton:
                textInfoState.setText("Saving on Cache Private Storage App  (" + this.getApplicationContext().getCacheDir () + "/" + FILE_NAME + ") for " + editTextUserName.getText());
                textInfoState.setTextColor(Color.YELLOW);
                saveCache();
                break;


            case com.keikis.logs.R.id.loadCacheStorageButton:
                textInfoState.setText("Loading from Cache Private Storage App (" + this.getApplicationContext().getCacheDir() + "/" + FILE_NAME + ") for " + editTextUserName.getText());
                textInfoState.setTextColor(Color.BLUE);
                loadCache();
                break;


            case com.keikis.logs.R.id.saveSqlite3Storage:
                textInfoState.setText("Saving on Sqlite3 DB for Key " + editTextUserName.getText());
                textInfoState.setTextColor(Color.YELLOW);
                saveSqlite3Storage();
                break;

            case com.keikis.logs.R.id.loadSqlite3Storage:
                textInfoState.setText("Loading on Sqlite3 DB for Key " + editTextUserName.getText());
                textInfoState.setTextColor(Color.BLUE);
                loadSqlite3Storage();
                break;

            case com.keikis.logs.R.id.deleteAllStorage:
                textInfoState.setText("Deleting All Storages: Internal File, External Private, Internal Cache, Sqlite3..." );
                textInfoState.setTextColor(Color.BLUE);
                deleleAllStorages();
                textInfoState.setText("Deleted All Storages: Internal File, External Private, Internal Cache, Sqlite3..." );
                break;

        }
    }

    private boolean isSavingAction(View v) {

        return (null!=v.getTag() && v.getTag().equals("save"));

    }

    private boolean isLoadActionFromDB(View v) {

        return (null!=v.getTag() && v.getTag().equals("loadDB"));

    }



    /**
     * Validation returns true or false in function of if all data is completed (not null nor blank string "")
     * and the age es numeric.
     */
    private boolean validateData(String userName, String userAge) throws IllegalArgumentException {

        return (null != userName) && (null != userAge) && (!userName.isEmpty()) && (!userAge.isEmpty()) && isNumeric(userAge);
    }

    private static boolean isNumeric(String str) {
        Log.v(TAG , "isNumeric: " + str.matches("-?\\d+(\\.\\d+)?") + " to access External Storage");
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    /**
     * Different methods for persistence data:
     * Save/Load preferences app
     * Save/Load internal storage into app bundle /data/data/com.keikis.persistence
     */
    private void savePreferences() {
        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("user_name", editTextUserName.getText().toString());
        editor.putInt("user_age", Integer.parseInt(editTextUserAge.getText().toString()));

        editTextUserName.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

        // Commit the edits!
        editor.commit();

    }


    private void loadPreferences() {

        // Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String userName = settings.getString("user_name", "");
        int userAge = settings.getInt("user_age", 0);

        editTextUserName.setText(userName);
        editTextUserAge.setText(String.valueOf(userAge));

        editTextUserName.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);


    }


    private void saveInternal() {

        // Open fileOutputStream and printwriter and write data username, userage
        FileOutputStream fos = null;
        PrintWriter filePrint = null;

        try {
            fos = openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            filePrint = new PrintWriter(fos);

            filePrint.println(editTextUserName.getText().toString());
            filePrint.println(editTextUserAge.getText().toString());

            Log.v(TAG , "saveInternal:  println " + editTextUserName.getText().toString() + "," + editTextUserName.getText().toString() );


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null!=filePrint) filePrint.close();
                if(null!=fos) fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        editTextUserName.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

    }


    private void loadInternal() {
        String userName = null, userAge = null;
        // Restore preferences
        // Open fileOutputStream and printwriter and write data username, userage
        FileReader fileReader = null;
        try {

            //Context.openFileInput(

            fileReader = new FileReader(this.getApplicationContext().getFilesDir() + "/" + FILE_NAME);
            BufferedReader br = new BufferedReader(fileReader);


            String line = null;
            int line_number = 0;
            // If no more lines the readLine() returns null
            while ((line = br.readLine()) != null) {
                // Reading lines until the end of the file
                // At first line we have the userName
                // At second line we have t he userAge
                Log.v(TAG , "loadInternal:  readLine read from internal " + line);
                if (line_number == 0) userName = line;
                else userAge = line;
                line_number++;

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            showErrorMsg("FileNotFoundException " + this.getApplicationContext().getFilesDir()  + "/" + FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null!=fileReader) fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(null!=userName)  editTextUserName.setText(userName);
        if(null!=userAge)   editTextUserAge.setText(String.valueOf(userAge));

        editTextUserName.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);

    }


    private void saveExternal() {

        // Open FileOutputStream and PrintWriter and write data username, userage
        FileOutputStream fos = null;
        PrintWriter filePrint = null;

        try {


            File file = new File(Environment.getExternalStorageDirectory().toString(), FILE_NAME);
            fos = new FileOutputStream(file);
            filePrint = new PrintWriter(fos);


            filePrint.println(editTextUserName.getText().toString());
            filePrint.println(editTextUserAge.getText().toString());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null!=filePrint) filePrint.close();
                if(null!=fos) fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        editTextUserName.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

    }


    private void loadExternal() {
        String userName = null, userAge = null;
        // Restore preferences
        // Open fileOutputStream and printwriter and write data username, userage
        FileReader fileReader = null;
        try {

            fileReader = new FileReader(Environment.getExternalStorageDirectory().toString() + "/" + FILE_NAME);
            BufferedReader br = new BufferedReader(fileReader);

            String line = null;
            int line_number = 0;
            // If no more lines the readLine() returns null
            while ((line = br.readLine()) != null) {
                // Reading lines until the end of the file
                // At first line we have the userName
                // At second line we have t he userAge
                if (line_number == 0) userName = line;
                else userAge = line;
                line_number++;

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            showErrorMsg("FileNotFoundException " + Environment.getExternalStorageDirectory().toString()  + "/" + FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null!=fileReader) fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(null!=userName)  editTextUserName.setText(userName);
        if(null!=userAge)  editTextUserAge.setText(String.valueOf(userAge));

        editTextUserName.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);

    }



    private void saveCache() {

        // Open fileOutputStream and printwriter and write data username, userage
        FileOutputStream fos = null;
        PrintWriter filePrint = null;
        File file = null;

        try {

            //if you need to cache some files, you must use createTempFile ().
            //file = File.createTempFile(FILE_NAME, null, this.getApplicationContext().getCacheDir());
            file = new File(this.getApplicationContext().getCacheDir(),FILE_NAME);

            fos = new FileOutputStream(file);
            filePrint = new PrintWriter(fos);

            filePrint.println(editTextUserName.getText().toString());
            filePrint.println(editTextUserAge.getText().toString());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null!=filePrint) filePrint.close();
                if(null!=fos) fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        editTextUserName.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

    }


    private void loadCache() {
        String userName = null, userAge = null;
        // Restore preferences
        // Open fileOutputStream and printwriter and write data username, user, age
        FileReader fileReader = null;
        try {

            fileReader = new FileReader(this.getApplicationContext().getCacheDir().toString() + "/" +  FILE_NAME);
            BufferedReader br = new BufferedReader(fileReader);

            String line = null;
            int line_number = 0;
            // If no more lines the readLine() returns null
            while ((line = br.readLine()) != null) {
                // Reading lines until the end of the file
                // At first line we have the userName
                // At second line we have t he userAge
                if (line_number == 0) userName = line;
                else userAge = line;
                line_number++;

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            showErrorMsg("FileNotFoundException " + this.getApplicationContext().getCacheDir().toString()  + "/" + FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null!=fileReader) fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(null!=userName) editTextUserName.setText(userName);
        if(null!=userAge) editTextUserAge.setText(String.valueOf(userAge));

        editTextUserName.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);

    }





    private void saveSqlite3Storage() {

        UserDBAdapter userDB=null;

        try {

            userDB = new UserDBAdapter(this);

            //Open DB
            userDB.open();


            //Create user with Name, Age, Fake, Pass, Admin Role and Description
            userDB.createUser(editTextUserName.getText().toString(), Integer.valueOf( editTextUserAge.getText().toString() ),
                              "pass", "Admin", "A Good Person");

        } finally {
            userDB.close();
        }

        editTextUserName.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);

    }


    private void loadSqlite3Storage() {
        String userName = null, userAge = null;

        UserDBAdapter userDB=null;


        try {

            userDB = new UserDBAdapter(this);

            //Open DB
            userDB.open();

            // Load From Database
            ContentValues userValue =  userDB.fetch(editTextUserName.getText().toString());

            if(userValue.get(DataBaseHelper.USER_KEY_PASS)!=null)
            {

                userName = userValue.get(DataBaseHelper.USER_KEY_NAME).toString();
                userAge =  userValue.get(DataBaseHelper.USER_KEY_AGE).toString();

            }
            else
            {

                showErrorMsg("A valid user name is necessary (not found in DataBase) " + editTextUserName.getText().toString());

            }



        } finally {
            userDB.close();
        }


        editTextUserName.setText(userName);
        editTextUserAge.setText(String.valueOf(userAge));

        editTextUserName.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
        editTextUserAge.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);

    }




    private void deleleAllStorages() {


        File file = null;


        //Delete (Clear) Preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();


       //Delete internal file
       file = new File(this.getApplicationContext().getFilesDir() + "/" + FILE_NAME);
       file.delete();

       //Delete private external file
       file = new File(Environment.getExternalStorageDirectory().toString() + "/" + FILE_NAME);
       file.delete();

       //Delete internal cache file
       file = new File(this.getApplicationContext().getCacheDir(), FILE_NAME);
       file.delete();

        //Sqlite3 Delete all Users
       UserDBAdapter userDB = new UserDBAdapter(this);
        userDB.open();
       userDB.deleteAllUsers();
    }









    /* ********************************************************************************************** */



    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read, if is writable then is readble */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "onRequestPermissionsResult: MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE PERMISSION_GRANTED ");

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    if (isExternalStorageWritable()) {    ;
                        saveExternal();
                    }


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "onRequestPermissionsResult: No permissions granted to write on External Storage");
                    showErrorMsg("No permissions granted to write on External Storage");
                }
                return;
            }


            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    if (isExternalStorageWritable()) {
                        loadExternal();
                    }


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                    showErrorMsg("No permissions granted to write on External Storage");
                }
                return;
            }


            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    private void showErrorMsg(String text) {

        textInfoState.setText(text);
        textInfoState.setTextColor(Color.RED);
        Animation anim = new AlphaAnimation(0.0f, 1.0f);  //Animation to blink efect in validation error
        anim.setDuration(3450); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        textInfoState.startAnimation(anim);

        Toast.makeText(this, text,
                Toast.LENGTH_LONG).show();
    }


}
