/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.logs.dbadapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DataBaseHelper extends SQLiteOpenHelper {

    //DB Create
    public static final String DATABASE_NAME = "User";
    public static final int DATABASE_VERSION = 1;


    //DB Create User
    public static final String SQLITE_TABLE_USER = "User";
    public static final String USER_KEY_ROWID = "_id";
    public static final String USER_KEY_NAME = "name";
    public static final String USER_KEY_AGE = "age";
    public static final String USER_KEY_PASS = "pass";
    public static final String USER_KEY_ROLE = "role";
    public static final String USER_KEY_DESCRIPTION = "description";



    private static final String USER_CREATE_TABLE_SENTENCE =
            "CREATE TABLE if not exists " + SQLITE_TABLE_USER + " (" +
                    USER_KEY_ROWID + " integer PRIMARY KEY autoincrement," +
                    USER_KEY_NAME  + "," +
                    USER_KEY_AGE   + "," +
                    USER_KEY_PASS  + "," +
                    USER_KEY_ROLE  + "," +
                    USER_KEY_DESCRIPTION + "," +
                    " UNIQUE (" + USER_KEY_NAME + "));";



    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(USER_CREATE_TABLE_SENTENCE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}