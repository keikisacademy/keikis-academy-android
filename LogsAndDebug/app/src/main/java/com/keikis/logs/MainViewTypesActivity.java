/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.logs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;



public class MainViewTypesActivity extends AppCompatActivity {
    public static final String VIEW_TO_LOAD = "viewToLoad";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.keikis.logs.R.layout.main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Create App Bar with menu options
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(com.keikis.logs.R.menu.bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    public void showViewDetails1(View view) {
        Intent intent = new Intent(this, PersistenceLogViewActivity.class);
        intent.putExtra(VIEW_TO_LOAD, com.keikis.logs.R.layout.persistence_view);
        startActivity(intent);
    }


}
