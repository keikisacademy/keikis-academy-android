![keikis Logo](media/keikis-academy-logo-small.png)

# Android MultiScreenAndLang

Easily explained the behaviour of the multi-screen support, you can understand what are DPs & DPIs and How apply qualifier for different orientations or devices. Also what are the cut of point in Smartphone, Fablets & Tablets and much more.
Also explain how develop a multi-language app.
Remember full Explanations and Examples could be located on [academy.keikis.org - android-base-multi-device-language-support](http://academy.keikis.org/android-base-multi-device-language-support/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Android Studio 2.3


### Installing

Easy!. Download and open the project from File> Open...


### Building

Easy Too!. Build > Make Project


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **keikis academy** [academy.keikis.org](http://academy.keikis.org)


## License

This project is licensed under [Creative Commons Attribution 4.0 Unported License](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)
