/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class RandomView2Activity extends AppCompatActivity {
    private SeekBar seekBar;
    private TextView TextViewState;
    private RatingBar ratingBar;
    private Spinner spinner;
    private Switch switchControl;
    private ToggleButton toggleButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);

        // Get intent extra data with the layout to load
        Intent intent = getIntent();
        int layout = intent.getIntExtra(MainViewTypesActivity.VIEW_TO_LOAD, 0);

        // Create a FrameLayout in run-time and clean views
        FrameLayout frame = (FrameLayout) findViewById(R.id.main_frame);
        frame.removeAllViews();

        // Instantiate the Inflater and load the layout from layout, load Create a FrameLayout in run-time and clean views
        LayoutInflater inflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        frame.addView(inflator.inflate(layout, null));

        // Get views from xml layout
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        TextViewState = (TextView) findViewById(R.id.textViewState);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        spinner = (Spinner) findViewById(R.id.spinner);
        switchControl = (Switch) findViewById(R.id.switchControl);
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);


        // SeekBar Listener - Handler seekbar and show the listener
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;
                Toast.makeText(getApplicationContext(), "Changing seekbar's progress", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Toast.makeText(getApplicationContext(), "Started tracking seekbar", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                TextViewState.setText("Covered: " + progress + "/" + seekBar.getMax());
                Toast.makeText(getApplicationContext(), "Stopped tracking seekbar", Toast.LENGTH_SHORT).show();
            }
        });


        // RatingBar Listener -  Display the current rating value in the result (textview) automatically
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                TextViewState.setText(String.valueOf(rating));

            }
        });


        // Spinner Listener - Display the current rating value in the result (textview) automatically
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                String spinnerText = adapter.getItemAtPosition(position).toString();
                // Showing selected spinner item
                Toast.makeText(getApplicationContext(),
                        "Selected Country : " + spinnerText, Toast.LENGTH_SHORT).show();
                TextViewState.setText(String.valueOf(spinnerText));
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        //attach a listener to check for changes in state
        switchControl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){
                    TextViewState.setText("Switch is currently ON");
                }else{
                    TextViewState.setText("Switch is currently OFF");
                }

            }
        });



        toggleButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(toggleButton.isChecked())
                    TextViewState.setText(toggleButton.getTextOn());
                else
                    TextViewState.setText(toggleButton.getTextOff());

            }
        });




    }


}




