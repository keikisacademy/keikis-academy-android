/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class MainViewTypesActivity extends AppCompatActivity {
    public static final String VIEW_TO_LOAD = "viewToLoad";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view_types);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Create App Bar with menu options
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_demos:
                // Start Activity Mix Demos
                Intent intent = new Intent(MainViewTypesActivity.this, MainMixedDemos.class);
                startActivity(intent);
                return true;

            case R.id.action_groups:
                // Start Activity Group Demos
                intent = new Intent(MainViewTypesActivity.this, ViewGroupsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_menus:
                // Start Activity Group Demos
                intent = new Intent(MainViewTypesActivity.this, MenusActivity.class);
                startActivity(intent);
                return true;

            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;

            default:
                // Nothing to do, pass control to menu
                return super.onOptionsItemSelected(item);

        }
    }


    public void showViewDetails1(View view) {
        Intent intent = new Intent(this, RandomView1Activity.class);
        intent.putExtra(VIEW_TO_LOAD, R.layout.random_views1);
        startActivity(intent);
    }

    public void showViewDetails2(View view) {
        Intent intent = new Intent(this, RandomView2Activity.class);
        intent.putExtra(VIEW_TO_LOAD, R.layout.random_views2);
        startActivity(intent);
    }

    public void showViewPadding(View view) {
        Intent intent = new Intent(this, PaddingActivity.class);
        intent.putExtra(VIEW_TO_LOAD, R.layout.padding);
        startActivity(intent);
    }

    public void showViewGroups(View view) {
        Intent intent = new Intent(this, ViewGroupsActivity.class);
        startActivity(intent);
    }

    public void showMenus(View view) {
        Intent intent = new Intent(this, MenusActivity.class);
        startActivity(intent);
    }

    public void otherDemos(View view) {
        Intent intent = new Intent(this, MainMixedDemos.class);
        startActivity(intent);
    }
}
