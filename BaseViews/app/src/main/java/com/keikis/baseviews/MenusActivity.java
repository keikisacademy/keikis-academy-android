/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import static android.widget.AdapterView.AdapterContextMenuInfo;
import static com.keikis.baseviews.MainViewTypesActivity.VIEW_TO_LOAD;

public class MenusActivity extends AppCompatActivity {
    Button buttonMenu;
    ArrayAdapter<String> arrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        buttonMenu = (Button) findViewById(R.id.showMenuButton);//your menu button
        ListView listView = (ListView) findViewById(R.id.list);

        String[] countries = getResources().getStringArray(R.array.country_arrays);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, new ArrayList(Arrays.asList(countries)));
        listView.setAdapter(arrayAdapter);
        registerForContextMenu(listView);

        buttonMenu.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(MenusActivity.this, buttonMenu);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

                //Registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(MenusActivity.this, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();

                        switch (item.getItemId()) {
                            case R.id.action_drawable_animation:
                                // Start Activity AnimationDrawableList
                                Intent intent = new Intent(MenusActivity.this, AnimationDrawableList.class);
                                startActivity(intent);
                                break;
                            case R.id.action_image_button:
                                // Start Activity ImageButtonActivity
                                intent = new Intent(MenusActivity.this, ImageButtonActivity.class);
                                startActivity(intent);
                                break;
                            case R.id.action_random_views_1:
                                // Start Activity RandomView1Activity
                                intent = new Intent(MenusActivity.this, RandomView1Activity.class);
                                intent.putExtra(VIEW_TO_LOAD, R.layout.random_views1);
                                startActivity(intent);
                                break;
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });//closing the setOnClickListener method


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Toast.makeText(this, "Click on button to show a Popup Menu.\n\nLong Click on list item to show a Context Menu.", Toast.LENGTH_LONG).show();
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // Get more info about the menu item
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        // Get the view target and text into the element
        String key = ((TextView) info.targetView).getText().toString();
        switch (item.getItemId()) {
            case R.id.action_remove:
                // Remove Item
                Toast.makeText(this, "Remove Item " + item.getTitle(), Toast.LENGTH_SHORT).show();
                arrayAdapter.remove(arrayAdapter.getItem(info.position));
                return true;

            case R.id.action_country:
                // Selected Item
                Toast.makeText(this, "Selected Item " + key, Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Create App Bar with menu options
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_demos:
                // Start Activity Mix Demos
                Intent intent = new Intent(MenusActivity.this, MainMixedDemos.class);
                startActivity(intent);
                return true;

            case R.id.action_groups:
                // Start Activity Group Demos
                intent = new Intent(MenusActivity.this, ViewGroupsActivity.class);
                startActivity(intent);
                return true;

            case R.id.action_menus:
                // Start Activity Group Demos
                intent = new Intent(MenusActivity.this, MenusActivity.class);
                startActivity(intent);
                return true;

            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;

            default:
                // Nothing to do, pass control to menu
                return super.onOptionsItemSelected(item);

        }
    }

}
