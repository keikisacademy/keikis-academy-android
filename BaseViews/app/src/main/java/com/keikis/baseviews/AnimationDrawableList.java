/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class AnimationDrawableList extends Activity {
    AnimationDrawable animationMan;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Get the resouce animation.xml
        animationMan = (AnimationDrawable) getResources().getDrawable(R.drawable.animation_man, null);
        //Create a view with a image
        ImageView image = new ImageView(this);

        image.setBackgroundColor(Color.WHITE);

        //Set the animation to imageView
        image.setImageDrawable(animationMan);
        image.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        //Set the view to activity content
        setContentView(image);

        Toast.makeText(this, "Touch to Animate!", Toast.LENGTH_SHORT).show();
    }

    public boolean onTouchEvent(MotionEvent evento) {
        //The animation start at touch the screen
        if (evento.getAction() == MotionEvent.ACTION_DOWN) {
            //Start the animation
            animationMan.start();
            return true;
        }
        return super.onTouchEvent(evento);
    }
}