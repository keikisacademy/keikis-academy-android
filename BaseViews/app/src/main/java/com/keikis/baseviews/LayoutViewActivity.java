/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

public class LayoutViewActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);

        // Get intent extra data with the layout to load
        Intent intent = getIntent();
        int layout = intent.getIntExtra(MainViewTypesActivity.VIEW_TO_LOAD, 0);

        // Create a FrameLayout in run-time and clean views
        FrameLayout frame = (FrameLayout) findViewById(R.id.main_frame);
        frame.removeAllViews();

        // Instantiate the Inflater and load the layout from layout, load Create a FrameLayout in run-time and clean views
        LayoutInflater inflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        frame.addView(inflator.inflate(layout, null));

    }


}
