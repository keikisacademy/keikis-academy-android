/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

public class AnimationTween extends Activity {

    Animation animationRotateCenter;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_tween);

        Toast.makeText(this, "Click button to Animate!", Toast.LENGTH_SHORT).show();

        final Button buttonRotateCenter = (Button) findViewById(R.id.buttonAnimationTween);

        animationRotateCenter = AnimationUtils.loadAnimation(this, R.anim.rotate_center);
        buttonRotateCenter.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                buttonRotateCenter.startAnimation(animationRotateCenter);
            }
        });

    }


}