/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

public class RandomView1Activity extends AppCompatActivity {


    Button buttonSayHello;
    EditText editTextSayHello;
    TextView textViewSayHello;
    TextView textTitle;
    RadioButton radioSayHello;
    CheckBox checkBoxSayHello;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);

        // Get intent extra data with the layout to load
        Intent intent = getIntent();
        int layout = intent.getIntExtra(MainViewTypesActivity.VIEW_TO_LOAD, 0);

        // Create a FrameLayout in run-time and clean views
        FrameLayout frame = (FrameLayout) findViewById(R.id.main_frame);
        frame.removeAllViews();

        // Instantiate the Inflater and load the layout from layout, load Create a FrameLayout in run-time and clean views
        LayoutInflater inflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        frame.addView(inflator.inflate(layout, null));


        Configuration configuration = this.getResources().getConfiguration();
        int screenWidthDp = configuration.screenWidthDp; //The current width of the available screen space, in dp units, corresponding to screen width resource qualifier.
        int smallestScreenWidthDp = configuration.smallestScreenWidthDp; //The smallest screen size an application will see in normal operation, corresponding to smallest screen width resource qualifier.

        float xdpi = getResources().getDisplayMetrics().xdpi;
        float ydpi = getResources().getDisplayMetrics().ydpi;


        //Get the orientation and create string
        int orientation =   this.getResources().getConfiguration().orientation;
        String orientationString;
        if(orientation==Configuration.ORIENTATION_PORTRAIT){
            //code for portrait mode
            orientationString = "Portrait";
        }
        else{
            //code for landscape mode
            orientationString = "Landscape";
        }




        // Get views from xml layout
        buttonSayHello = (Button) findViewById(R.id.buttonSayHello);
        editTextSayHello = (EditText) findViewById(R.id.editSayHello);
        textViewSayHello = (TextView) findViewById(R.id.textSayHello);
        radioSayHello = (RadioButton) findViewById(R.id.radioSayHello);
        checkBoxSayHello = (CheckBox) findViewById(R.id.checkSayHello);

        textTitle = (TextView) findViewById(R.id.textTitle);

        textTitle.setText("Multi Screen Demo. Current Width DP: " + screenWidthDp + " smallestScreenWithDP: " + smallestScreenWidthDp +
                "\n" + "   Resolution x,y: "  + getScreenResolution(this) +
                "\n" + "   Orientation:  "  +  orientationString +
                " | "  + "Density:  "  +  getDensity(this.getResources()) +
                " | "  + " xDPI: "  + xdpi +
                ","    + "yDPI: "  + ydpi);


        // General View Listener - Handler the button onclick & change the textViewSayHello with the content with editTextSayHello
        View.OnClickListener myButtonSayHellohandler = new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.buttonSayHello:
                        textViewSayHello.setText(editTextSayHello.getText());
                        break;
                    case R.id.radioSayHello:
                        textViewSayHello.setText("RadioButton " + radioSayHello.isChecked());
                        break;
                    case R.id.checkSayHello:
                        textViewSayHello.setText("CheckBox " + checkBoxSayHello.isChecked());
                        break;
                }
            }
        };

        // Set myButtonSayHellohandler
        buttonSayHello.setOnClickListener(myButtonSayHellohandler);
        radioSayHello.setOnClickListener((myButtonSayHellohandler));
        checkBoxSayHello.setOnClickListener((myButtonSayHellohandler));

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private static String getScreenResolution(Context context)
    {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        //Attention for get the real resolution
        display.getRealMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        return String.format("%d,%d", width, height);
    }


    public static String getDensity(Resources resources) {
        switch (resources.getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                return "ldpi";
            case DisplayMetrics.DENSITY_MEDIUM:
                return "mdpi";
            case DisplayMetrics.DENSITY_HIGH:
                return "hdpi";
            case DisplayMetrics.DENSITY_XHIGH:
                return "xhdpi";
            case DisplayMetrics.DENSITY_XXHIGH:
                return "xxhdpi";
            case DisplayMetrics.DENSITY_XXXHIGH:
                return "xxxhdpi";
            case DisplayMetrics.DENSITY_TV:
                return "tvdpi";
            default:
                return "unknown";
        }
    }


}
