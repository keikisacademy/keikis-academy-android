/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import static com.keikis.baseviews.MainViewTypesActivity.VIEW_TO_LOAD;

public class ViewGroupsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_groups);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void showLinearHorizontalLayout(View view) {
        startDemo(R.layout.linear_horizontal);
    }

    public void showLinearVerticalLayout(View view) {
        startDemo(R.layout.linear_vertical);
    }

    public void showTableLayout(View view) {
        startDemo(R.layout.table_layout);
    }

    public void showRelativeLayout(View view) {
        startDemo(R.layout.relative_layout);
    }

    public void showScrollLayout(View view) {
        startDemo(R.layout.scroll_layout);
    }

    public void showFrameLayout(View view) {
        startDemo(R.layout.frame_layout);
    }

    private void startDemo(int layout) {
        Intent intent = new Intent(this, LayoutViewActivity.class);
        intent.putExtra(VIEW_TO_LOAD, layout);

        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;

            default:
                // Nothing to do, pass control to menu
                return super.onOptionsItemSelected(item);

        }
    }
}
