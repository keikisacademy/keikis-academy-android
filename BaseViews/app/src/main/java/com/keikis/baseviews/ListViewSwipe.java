/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.keikis.adapter.SwipeAdapter;

public class ListViewSwipe extends AppCompatActivity {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.list_view_swipe);

    ListView listView = (ListView) findViewById(R.id.list);
    listView.setAdapter(new SwipeAdapter(this, R.layout.swipe_list_view_item, getResources().getStringArray(R.array.country_arrays)));
  }
}
