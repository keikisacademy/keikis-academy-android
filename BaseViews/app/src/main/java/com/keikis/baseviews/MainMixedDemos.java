/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainMixedDemos extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_mixed_demos);
  }

  public void startAnimationTranstion(View view) {
    Intent intent = new Intent(this, AnimationTransitionManager.class);
    startActivity(intent);
  }


  public void startAnimationTween(View view) {
    Intent intent = new Intent(this, AnimationTween.class);
    startActivity(intent);
  }


  public void startAnimationListDrawables(View view) {
    Intent intent = new Intent(this, AnimationDrawableList.class);
    startActivity(intent);
  }

  public void refreshView(View view) {
    Intent intent = new Intent(this, SwipeRefresh.class);
    startActivity(intent);
  }

  public void listViewSwipe(View view) {
    Intent intent = new Intent(this, ListViewSwipe.class);
    startActivity(intent);
  }

  public void stackedView(View view) {
    Intent intent = new Intent(this, StackedViewActivity.class);
    startActivity(intent);
  }

  public void imageView(View view) {
    Intent intent = new Intent(this, ImageViewActivity.class);
    startActivity(intent);
  }

  public void imageButton(View view) {
    Intent intent = new Intent(this, ImageButtonActivity.class);
    startActivity(intent);
  }
}
