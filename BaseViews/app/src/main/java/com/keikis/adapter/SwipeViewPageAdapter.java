/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.keikis.baseviews.DeleteItemFragment;
import com.keikis.baseviews.ListItemFragment;

import java.util.List;


public class SwipeViewPageAdapter extends FragmentPagerAdapter {

  private final List<String> stringArray;
  private final int position;
  private final SwipeAdapter swipeAdapter;

  public SwipeViewPageAdapter(FragmentManager fm, List<String> stringArray, int position, SwipeAdapter swipeAdapter) {
    super(fm);
    this.stringArray = stringArray;
    this.position = position;
    this.swipeAdapter = swipeAdapter;
  }

  @Override
  public Fragment getItem(final int position) {
    if (position == 0) {
      ListItemFragment listItemFragment = new ListItemFragment();
      Bundle bundle = new Bundle();
      bundle.putString("value", stringArray.get(this.position));
      listItemFragment.setArguments(bundle);
      return listItemFragment;
    } else {
      final DeleteItemFragment deleteItemFragment = new DeleteItemFragment();
      Bundle bundle = new Bundle();
      bundle.putInt("position", this.position);
      deleteItemFragment.setArguments(bundle);
      deleteItemFragment.setAdapter(this);
      return deleteItemFragment;
    }
  }

  public void deleteItemFromList() {
    Log.d("delete", "View Page item:" + this.position);
    swipeAdapter.deleteItem(this.position);
  }

  @Override
  public int getCount() {
    return 2;
  }
}