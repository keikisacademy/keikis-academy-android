/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.baseviews;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
  public ApplicationTest() {
    super(Application.class);
  }
}