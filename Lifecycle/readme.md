![keikis Logo](media/keikis-academy-logo-small.png)

# Android Lifecycle

Easily explained the complete lifecycle of an activity and shows how is executed each state in function of the activity was started, restarted, resume, stop, finish,... when other activity is invoked.
All clearly with traces using logcat for each handler.
And the most important Remember full Explanations and Examples could be located on [academy.keikis.org - android-activity-intents](http://academy.keikis.org/android-activity/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Android Studio 2.3


### Installing

Easy!. Download and open the project from File> Open...


### Building

Easy Too!. Build > Make Project


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

* **keikis academy** [academy.keikis.org](http://academy.keikis.org)


## License

This project is licensed under [Creative Commons Attribution 4.0 Unported License](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)
