/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.lifecycle;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

public class InitActivityOne extends AppCompatActivity {

    final  String TAG = "InitActivityOne"; 

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_one);
        Log.d(TAG, "Lifecyle  onCreate()");
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "Lifecyle  onRestart()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Lifecyle  onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Lifecyle  onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "Lifecyle  onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "Lifecyle  onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Lifecyle  onDestroy()");
    }


    /** Called when the user clicks the Go button */
    public void goActivityTwo(View view) {
        Intent intent = new Intent(this, InitActivityTwo.class);
        startActivity(intent);
    }
}
