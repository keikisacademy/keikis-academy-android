/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.intentparameters;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import static android.widget.Toast.LENGTH_LONG;

public class InitActivityOne extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.example.intentparameter.MESSAGE";
    private static final int PICK_COMPLETE_MESSAGE_REQUEST = 1;  // The request code PICK_COMPLETE_MESSAGE
    private static final int PICK_CONTACT_REQUEST = 2;  // The request code PICK_CONTACT_REQUEST
    private static final int MY_CALL_PERMISSIONS_REQUEST = 3;
    private static final int MY_READ_CALENDAR_REQUEST = 4;
    final String TAG = "InitActivityOne";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_one);
        Log.d(TAG, "IntentParameters  onCreate()");

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "IntentParameters  onRestart()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "IntentParameters  onStart()");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "IntentParameters  onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "IntentParameters  onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "IntentParameters  onStop()");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "IntentParameters  onDestroy()");
    }


    /**
     * Called when the user clicks the Go Contact Button
     */
    public void pickContact(View view) {

        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT_REQUEST);

    }

    public void invokeWebBrowser(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://www.google.com"));
        startActivity(intent);
    }

    public void invokeWebSearch(View view) {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.setData(Uri.parse("http://www.google.com"));
        intent.putExtra(SearchManager.QUERY, "Mobile");
        try {
            startActivity(intent);
        } catch (Exception e)  //Not found activity
        {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void dial(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        startActivity(intent);
    }

    public void call(View view) {

        //Call phone is not a dangerous permission, no check rationale is requered
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MY_CALL_PERMISSIONS_REQUEST);
            return;
        } else callPhone();
    }


    public void calendar(View view) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CALENDAR)) {
                Log.d(TAG, "calendar shouldShowRequestPermissionRationale returned true");
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setCancelable(true);
                alertBuilder.setMessage("Read Calendar permission is necessary to access to your Happy Date!");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(InitActivityOne.this, new String[]{Manifest.permission.READ_CALENDAR}, MY_READ_CALENDAR_REQUEST);
                        return;
                    }
                });
                alertBuilder.show();
            } else {
                ActivityCompat.requestPermissions(InitActivityOne.this, new String[]{Manifest.permission.READ_CALENDAR}, MY_READ_CALENDAR_REQUEST);
                return;
            }
        } else readCalendar();
    }


    private void callPhone() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:555-555-555"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            startActivity(intent);
        }
    }



    private void readCalendar() {
        Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
        builder.appendPath("time");
        ContentUris.appendId(builder, java.util.Calendar.getInstance().getTimeInMillis());
        Intent intent = new Intent(Intent.ACTION_VIEW)
                .setData(builder.build());
        startActivity(intent);
    }

    public void showMapAtLatLong(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:0,0?z=4&q;=restaurantes"));
        startActivity(intent);
    }


    /**
     * Called when the user clicks the Go button
     */
    public void goActivityTwo(View view) {
        Intent intent = new Intent(this, InitActivityTwo.class);

        //Get data from edit text extra the name of the extra data, with package prefix to avoid collisions
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();

        //Set extra the name of the extra data, with package prefix to avoid collisions
        intent.putExtra(EXTRA_MESSAGE, message);

        startActivityForResult(intent, PICK_COMPLETE_MESSAGE_REQUEST);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        Log.d(TAG, "IntentParameters  onActivityResult() resultCode: " + resultCode);

        // Make sure the request was successful
        if (resultCode == RESULT_OK) {

            // Trace the result
            Log.d(TAG, "IntentParameters  onActivityResult() RESULT_OK");
            if (requestCode == PICK_COMPLETE_MESSAGE_REQUEST) {
                Log.d(TAG, "IntentParameters  onActivityResult() PICK_COMPLETE_MESSAGE_REQUEST " + data.getDataString());
            }

            // Check which request we're responding to
            if (requestCode == PICK_CONTACT_REQUEST) {
                // Make sure the request was successful

                Uri uri = data.getData();
                String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};

                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                int numberColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                String number = cursor.getString(numberColumnIndex);

                int nameColumnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String name = cursor.getString(nameColumnIndex);

                Log.d(TAG, "IntentParameters  onActivityResult() PICK_CONTACT_REQUEST number: " + number + " , name : " + name);

                Toast.makeText(this, "IntentParameters  onActivityResult() PICK_CONTACT_REQUEST \n Number: " + number + " , \n Name : " + name, LENGTH_LONG).show();


            }


            Log.d(TAG, "IntentParameters  onActivityResult() PICK_CONTACT_REQUEST " + data.getDataString());

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_CALL_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPhone();
                }
            }
            case MY_READ_CALENDAR_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    readCalendar();
                }
            }
        }
    }


}
