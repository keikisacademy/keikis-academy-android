/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.intentparameters;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class InitActivityTwo extends AppCompatActivity {

    final  String TAG = "InitActivityTwo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_two);
        Log.d(TAG, "IntentParameters  onCreate()");

        Intent intent = getIntent();
        String message = intent.getStringExtra(InitActivityOne.EXTRA_MESSAGE);

        //Creating a textview in run-time (at fly)
        TextView textView = new TextView(this);
        textView.setTextSize(30);
        textView.setText(message);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        textView.setGravity(Gravity.CENTER);


        //Getting layout of the activity and adding the before textview
        ViewGroup layout = (ViewGroup) findViewById(R.id.activity_init_two);
        layout.addView(textView);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "IntentParameters  onRestart()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "IntentParameters  onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "IntentParameters  onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "IntentParameters  onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "IntentParameters  onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "IntentParameters  onDestroy()");
    }


    /** Called when the user clicks the Go button */
    public void goActivityTwo(View view) {
        Intent intent = new Intent(this, InitActivityTwo.class);
        startActivity(intent);
    }


}
