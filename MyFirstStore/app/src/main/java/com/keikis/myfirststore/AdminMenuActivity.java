/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.keikis.myfirstapp.R;

public class AdminMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_menu);
    }

    public void moveToUser(View view){
        Intent intent = new Intent(AdminMenuActivity.this, ListUserActivity.class);
        startActivity(intent);
    }

    public void moveToProduct(View view){
        Intent intent = new Intent(AdminMenuActivity.this, ListProductActivity.class);
        startActivity(intent);
    }

    public void moveToPurchase(View view){
        Intent intent = new Intent(AdminMenuActivity.this, ListPurchaseActivity.class);
        startActivity(intent);
    }
}
