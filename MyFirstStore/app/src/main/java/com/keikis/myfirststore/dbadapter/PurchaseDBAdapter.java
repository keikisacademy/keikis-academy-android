/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore.dbadapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class PurchaseDBAdapter {

    private DataBaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    private final Context mCtx;

    public PurchaseDBAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public PurchaseDBAdapter open() throws SQLException {
        mDbHelper = new DataBaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long createPurchase(String nameUser, String nameProduct) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(DataBaseHelper.PURCHASE_KEY_USER, nameUser);
        initialValues.put(DataBaseHelper.PURCHASE_KEY_PRODUCT, nameProduct);


        return mDb.insert(DataBaseHelper.SQLITE_TABLE_PURCHASE, null, initialValues);
    }

    public boolean deleteAllPurchases() {
        int doneDelete = 0;
        doneDelete = mDb.delete(DataBaseHelper.SQLITE_TABLE_PURCHASE, null, null);
        return doneDelete > 0;

    }

    public Cursor fetchPurchases(String nameUser, String nameProduct) {
        //Query with join clause
        Cursor cur = mDb.rawQuery("SELECT Purchases._id, Purchases.nameUser, Purchases.nameProduct, Product.price, Purchases.time " +
                                  "FROM Purchases, Product WHERE nameUser like ? AND nameProduct like ? " +
                                  "     AND (Purchases.nameProduct = Product.name);",
                new String[]{String.valueOf("%"+(nameUser.toLowerCase()+"%")),String.valueOf("%"+(nameProduct)+"%")});
        Log.v("Cursor query", DatabaseUtils.dumpCursorToString(cur));
        Log.v("Cursor fetchPurchases", DatabaseUtils.dumpCursorToString(cur));
        //Move cursor at first position
        if (cur != null) {
            cur.moveToFirst();
        }
        return cur;
    }


    public Cursor fetchAllPurchases(){
        Cursor cur = mDb.rawQuery("SELECT Purchases._id, Purchases.nameUser, Purchases.nameProduct, Product.price, Purchases.time " +
                                   "FROM Purchases, Product;",null);
        // Move the cursor to the first position and read it
        // if empty, returns false
        if (cur != null) {
            cur.moveToFirst();
        }
        return cur;
    }


    public void insertSomePurchases() {
        createPurchase("user1", "Flower");
        createPurchase("user1", "Robot");
        createPurchase("user2", "Flower");
        createPurchase("user2", "Robot");
        createPurchase("user3", "Flower");
        createPurchase("user3", "Robot");
        createPurchase("user3", "Game Console");
        createPurchase("user3", "Car");
    }

}

