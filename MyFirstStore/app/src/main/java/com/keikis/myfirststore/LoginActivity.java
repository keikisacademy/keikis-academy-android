/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.keikis.myfirstapp.R;
import com.keikis.myfirststore.dbadapter.DataBaseHelper;
import com.keikis.myfirststore.dbadapter.PurchaseDBAdapter;
import com.keikis.myfirststore.dbadapter.UserDBAdapter;
import com.keikis.myfirststore.gobalappdata.GlobalAppData;

public class LoginActivity extends AppCompatActivity {


    EditText nameUser;
    EditText passUser;
    UserDBAdapter userDB=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

         nameUser = (EditText) findViewById(R.id.userName);
         passUser = (EditText) findViewById(R.id.passUser);

        userDB = new UserDBAdapter(this);

        //Open Users DataBase
        userDB.open();

        userDB.deleteAllUsers();

        userDB.insertSomeUsers();




        // *********************************** //
        // ************* RESETS ************** //
        // *********************************** //
        //Resets Purchases
        PurchaseDBAdapter dbPurchaseAdapter = new PurchaseDBAdapter(this);
        //Open Purchase DataBase
        dbPurchaseAdapter.open();
        //Clean Data
        dbPurchaseAdapter.deleteAllPurchases();
        //Add Data Purchases
        dbPurchaseAdapter.insertSomePurchases();
        //Close Data Base
        dbPurchaseAdapter.close();
    }


    public void loginAction(View view) {

        //Validations
        if(nameUser.getText().toString().matches("") || passUser.getText().toString().matches(""))
        {
            Toast.makeText(this, R.string.login_validaton_data_required, Toast.LENGTH_SHORT).show();
        }
        else
        {
            ContentValues userValue =  userDB.fetch(nameUser.getText().toString());

            if(userValue.get(DataBaseHelper.USER_KEY_PASS)!=null && !userValue.get(DataBaseHelper.USER_KEY_PASS).equals(passUser.getText().toString())) {

                showDialogError (getString(R.string.incorrect_user_validation));
            }
            else if(userValue.get(DataBaseHelper.USER_KEY_PASS)==null){
                showDialogError(getString(R.string.user_not_register));

            }else if(userValue.get(DataBaseHelper.USER_KEY_ROLE).equals("User")){

                //Login OK, save the user name & role in a global app context object
                final GlobalAppData globalAppData = (GlobalAppData) getApplicationContext();
                globalAppData.setUserName(userValue.get(DataBaseHelper.USER_KEY_NAME).toString());
                globalAppData.setUserRole(userValue.get(DataBaseHelper.USER_KEY_ROLE).toString());

                showDialogLoginUser();
            }

             else   {

                //Login OK, save the user name & role in a global app context object
                final GlobalAppData globalAppData = (GlobalAppData) getApplicationContext();
                globalAppData.setUserName(userValue.get(DataBaseHelper.USER_KEY_NAME).toString());
                globalAppData.setUserRole(userValue.get(DataBaseHelper.USER_KEY_ROLE).toString());

                showDialogLoginAdmin();
            }
        }

        return;

    }

    private void showDialogError(String s)
    {

        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Login")
                .setMessage(s)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void showDialogInfo(String s)
    {

        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Information")
                .setMessage(s)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // dismiss
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }


    private void showDialogLoginAdmin()
    {

        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Login")
                .setMessage("Are you sure you want to login?")

                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(LoginActivity.this, AdminMenuActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void showDialogLoginUser()
    {

        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Login")
                .setMessage("Are you sure you want to login?")

                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(LoginActivity.this, ListUserProductActivity.class);

                        //Login OK, save the user name in a global app context object
                        final GlobalAppData globalAppData = (GlobalAppData) getApplicationContext();
                        globalAppData.setUserName(nameUser.getText().toString());

                        startActivity(intent);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void showDialogRegister()
    {
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Register")
                .setMessage(R.string.register)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        finishRegister();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_login:
                Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;


            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void registerAction(View view) {

        // Validations
        if(nameUser.getText().toString().matches("") || passUser.getText().toString().matches(""))
        {
            Toast.makeText(this, R.string.used, Toast.LENGTH_SHORT).show();
        }
        else
        {
            ContentValues userValue =  userDB.fetch(nameUser.getText().toString());

            if(userValue.get(DataBaseHelper.USER_KEY_NAME)!=null) {

                showDialogError (this.getResources().getString(R.string.doubled));
            }
            else
                showDialogRegister();
        }

        return;

    }

    private void finishRegister() {
        userDB.createUser(nameUser.getText().toString(),passUser.getText().toString(),"User","Person");
        showDialogInfo(nameUser.getText().toString()+" "+this.getResources().getString(R.string.created));
    }

}
