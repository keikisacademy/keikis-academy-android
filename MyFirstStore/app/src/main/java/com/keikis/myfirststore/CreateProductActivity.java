/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.keikis.myfirstapp.R;
import com.keikis.myfirststore.dbadapter.ProductDBAdapter;

public class CreateProductActivity extends AppCompatActivity {

    EditText nameProduct;
    EditText priceProduct;
    EditText descriptionProduct;
    ProductDBAdapter productDB = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_product);

        nameProduct = (EditText) findViewById(R.id.nameProduct);
        priceProduct = (EditText) findViewById(R.id.priceProduct);
        descriptionProduct = (EditText) findViewById(R.id.descriptionProduct);
        productDB = new ProductDBAdapter(this);
        productDB.open();

    }


    public void createProductAction(View view) {

        //Validations
        if (nameProduct.getText().toString().matches("") || priceProduct.getText().toString().matches("")) {
            Toast.makeText(this, R.string.product_validation_ko, Toast.LENGTH_SHORT).show();
        } else {
            Cursor nameValue = productDB.fetchProductsByName(nameProduct.getText().toString());
            if (nameValue != null && nameValue.getCount() > 0) {
                Toast.makeText(this, R.string.product_already_exists, Toast.LENGTH_SHORT).show();
            } else {
                productDB.createProduct(nameProduct.getText().toString(),priceProduct.getText().toString(),descriptionProduct.getText().toString());
                Toast.makeText(this, R.string.new_product_ok, Toast.LENGTH_SHORT).show();
            }
        }
    }
}