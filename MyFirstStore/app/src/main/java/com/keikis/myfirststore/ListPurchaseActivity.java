/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.keikis.myfirstapp.R;
import com.keikis.myfirststore.dbadapter.DataBaseHelper;
import com.keikis.myfirststore.dbadapter.PurchaseDBAdapter;
import com.keikis.myfirststore.dbadapter.UserDBAdapter;
import com.keikis.myfirststore.gobalappdata.GlobalAppData;

public class ListPurchaseActivity extends AppCompatActivity {

    private PurchaseDBAdapter dbPurchaseAdapter;
    private UserDBAdapter userDBAdapter;

    private SimpleCursorAdapter dataAdapter;
    private EditText myFilterProductName;
    private EditText myFilterUserName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_purchase);

        dbPurchaseAdapter = new PurchaseDBAdapter(this);
        dbPurchaseAdapter.open();

        userDBAdapter = new UserDBAdapter(this);
        userDBAdapter.open();

        myFilterProductName = (EditText) findViewById(R.id.myFilterProductName);
        myFilterUserName = (EditText) findViewById(R.id.myFilterUserName);


        displayListView();
    }

    private void displayListView() {



        final GlobalAppData globalAppData = (GlobalAppData) getApplicationContext();
        String userNameLogged = globalAppData.getUserName();
        String userRoleLogged = globalAppData.getUserRole();
        Cursor cursor;
        final Spinner spnUsers = (Spinner) findViewById(R.id.userSpinner);



        // Show Purchases
        if (userRoleLogged.equals("Admin")) {
            //Load user spinner
            Cursor cursor_Names = userDBAdapter.fetchAllUsers();

            String[] columnsSpinner = new String[]{DataBaseHelper.USER_KEY_NAME};
            int[] toSpinner = new int[]{android.R.id.text1};

            SimpleCursorAdapter mAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_spinner_item, cursor_Names, columnsSpinner, toSpinner, 0);
            mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spnUsers.setAdapter(mAdapter);

            spnUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    Cursor cursor = (Cursor) spnUsers.getItemAtPosition(position);
                    myFilterUserName.setText(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.USER_KEY_NAME)));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            cursor = dbPurchaseAdapter.fetchAllPurchases();

        }
        else {

            myFilterUserName.setText(userNameLogged);
            myFilterUserName.setEnabled(false);
            spnUsers.setVisibility(View.GONE);

            cursor = dbPurchaseAdapter.fetchPurchases(userNameLogged,"");

        }



        // Set DB Fields
        String[] columns = new String[]{
                DataBaseHelper.PURCHASE_KEY_USER,
                DataBaseHelper.PURCHASE_KEY_PRODUCT,
                DataBaseHelper.PRODUCT_KEY_PRICE,
                DataBaseHelper.PURCHASE_KEY_TIME
        };

        // Set xml layout fields
        int[] to = new int[]{
                R.id.nameUser,
                R.id.nameProduct,
                R.id.priceProduct,
                R.id.time
        };

        // Create the adapter mapping cursor data columns with destinies (to layouts)
        dataAdapter = new SimpleCursorAdapter(this, R.layout.purchase_info, cursor, columns, to, 0);
        ListView listView = (ListView) findViewById(R.id.listView1);
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                // Get data from list position
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                // Get name
                String productCode = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.PURCHASE_KEY_PRODUCT));
                Toast.makeText(getApplicationContext(),
                        productCode, Toast.LENGTH_SHORT).show();
            }
        });



        //Add a general listener for filter-textfield
        myFilterProductName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                //Join with username with separator  _
                dataAdapter.getFilter().filter(myFilterUserName.getText().toString() + " _ "+ s.toString());
            }
        });

        //Set  un listener al textfield para filtrar
        myFilterUserName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                dataAdapter.getFilter().filter(s.toString() + " _ " + myFilterProductName.getText().toString());
            }
        });

        //Set filter function
        dataAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                //We have built a key with a separator, the first item is the product name key
                String[] result = constraint.toString().split("_");
                return dbPurchaseAdapter.fetchPurchases(result[0].trim(), result[1].trim());
            }
        });


    }
}
