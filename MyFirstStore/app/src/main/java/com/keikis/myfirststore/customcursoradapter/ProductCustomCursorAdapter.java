/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore.customcursoradapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.keikis.myfirstapp.R;
import com.keikis.myfirststore.dbadapter.PurchaseDBAdapter;
import com.keikis.myfirststore.gobalappdata.GlobalAppData;

public class ProductCustomCursorAdapter extends SimpleCursorAdapter {
    private Context mContext;
    private Context appContext;
    private int layout;
    private Cursor cr;
    private final LayoutInflater inflater;

    private TextView name;

    public ProductCustomCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flag) {
        super(context,layout,c,from,to,flag);
        this.layout=layout;
        this.mContext = context;
        this.inflater=LayoutInflater.from(context);
        this.cr=c;
    }



    @Override
    public void bindView(final View view, final Context context, final Cursor cursor){
        //int row_id = cursor.getPosition();
        //get('_id');  //Your row id (might need to replace)

        super.bindView(view, context, cursor);

        Button button = (Button) view.findViewById(R.id.addCartButton);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                // Get name from cursor data from the list position
               // String productCode = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.PRODUCT_KEY_NAME));
                String productCode = ((TextView) view.findViewById(R.id.nameProduct)).getText().toString();

                Toast.makeText(v.getContext(), "Added Product: " + ((TextView) view.findViewById(R.id.nameProduct)).getText().toString(), Toast.LENGTH_SHORT).show();

                PurchaseDBAdapter dbPurchaseAdapter = new PurchaseDBAdapter(context);
                dbPurchaseAdapter.open();

                final GlobalAppData globalAppData = (GlobalAppData) context.getApplicationContext();

                dbPurchaseAdapter.createPurchase(globalAppData.getUserName(), productCode);

            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        return super.getView(position, convertView, parent);
    }


    /* @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return super.newView(context, cursor, parent);
    } */

    @Override
    public View newView (Context context, Cursor cursor, ViewGroup parent) {
        View v = inflater.inflate( R.layout.product_info_user, parent, false);
        // edit: no need to call bindView here. That's done automatically
        return v;
    }

/*
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.user_product, parent, false);
        bindView(v,context,cursor);
        return v;
    }




    class MySimpleCursorAdapter extends SimpleCursorAdapter {
    public MySimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView != null) {
            return convertView;
        }

        return LayoutInflater.from(context).inflate(R.layout.listform_item);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        String name = cursor.getString(cursor.getColumnIndex(DBHelper.FORM_NAME));

        TextView text = (TextView) findViewById(R.id.tvFormName);
        text.setText(name);

        Button yourButton = (Button) findViewById(R.id.ibtnDelete);
        yourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }
}
     */
}