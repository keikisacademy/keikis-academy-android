/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore.dbadapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class ProductDBAdapter {

    private static final String TAG = "ProductsDbAdapter";
    private DataBaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    private final Context mCtx;

    public ProductDBAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public ProductDBAdapter open() throws SQLException {
        mDbHelper = new DataBaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public long createProduct(String name, String price,
                              String description) {

        ContentValues initialValues = new ContentValues();
        initialValues.put(DataBaseHelper.PRODUCT_KEY_NAME, name);
        initialValues.put(DataBaseHelper.PRODUCT_KEY_PRICE, price);
        initialValues.put(DataBaseHelper.PRODUCT_KEY_DESCRIPTION, description);

        return mDb.insert(DataBaseHelper.SQLITE_TABLE_PRODUCT, null, initialValues);
    }

    public boolean deleteAllProducts() {

        int doneDelete = 0;
        doneDelete = mDb.delete(DataBaseHelper.SQLITE_TABLE_PRODUCT, null, null);
        return doneDelete > 0;

    }

    public boolean deleteProductID(String productID) {

        int doneDelete = 0;
        doneDelete = mDb.delete(DataBaseHelper.SQLITE_TABLE_PRODUCT,  DataBaseHelper.PRODUCT_KEY_ROWID + "=" + productID , null);
        return doneDelete > 0;

    }

    public Cursor fetchProductsByName(String inputText) throws SQLException {
        Log.w(TAG, inputText);
        Cursor mCursor = null;
        if (inputText == null || inputText.length() == 0) {
            mCursor = mDb.query(DataBaseHelper.SQLITE_TABLE_PRODUCT, new String[]{DataBaseHelper.PRODUCT_KEY_ROWID,
                            DataBaseHelper.PRODUCT_KEY_NAME, DataBaseHelper.PRODUCT_KEY_PRICE, DataBaseHelper.PRODUCT_KEY_DESCRIPTION},
                    null, null, null, null, null);

        } else {
            mCursor = mDb.query(true, DataBaseHelper.SQLITE_TABLE_PRODUCT, new String[]{DataBaseHelper.PRODUCT_KEY_ROWID,
                            DataBaseHelper.PRODUCT_KEY_NAME, DataBaseHelper.PRODUCT_KEY_PRICE, DataBaseHelper.PRODUCT_KEY_DESCRIPTION},
                    DataBaseHelper.PRODUCT_KEY_NAME + " like '%" + inputText + "%'", null,
                    null, null, null, null);
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor fetchAllProducts() {
        //Query
        Cursor mCursor = mDb.query(DataBaseHelper.SQLITE_TABLE_PRODUCT, new String[]{DataBaseHelper.PRODUCT_KEY_ROWID,
                        DataBaseHelper.PRODUCT_KEY_NAME, DataBaseHelper.PRODUCT_KEY_PRICE, DataBaseHelper.PRODUCT_KEY_DESCRIPTION},
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public void insertSomeProducts() {

        createProduct("Flower","3.99€","Tulip");
        createProduct("Robot","4.99€" ,"Talking Robot");
        createProduct("Game Console", "9.99€", "Great games action & adventure!");
        createProduct("Car", "4.99€","Small smart car");
        createProduct("Flying Bird", "2.99€", "Beautiful bird");


    }

}