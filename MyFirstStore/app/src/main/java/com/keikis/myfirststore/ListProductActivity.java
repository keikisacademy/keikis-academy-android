/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.keikis.myfirstapp.R;
import com.keikis.myfirststore.dbadapter.DataBaseHelper;
import com.keikis.myfirststore.dbadapter.ProductDBAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.keikis.myfirstapp.R.string.delete;

public class ListProductActivity extends AppCompatActivity {

    private ProductDBAdapter dbProductAdapter;
    private SimpleCursorAdapter dataAdapter;

    //Keep temporal purchases
    private String[] PurchaseArrayString;
    private List<String> PurchaseArray = new ArrayList<String>();




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_product);

        dbProductAdapter = new ProductDBAdapter(this);
        dbProductAdapter.open();

        //Clean data
        dbProductAdapter.deleteAllProducts();

        //Add some products
        dbProductAdapter.insertSomeProducts();


        Toast.makeText(this, R.string.tip_list_product, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate del menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_admin, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_introduce:
                Intent introduce = new Intent(ListProductActivity.this, CreateProductActivity.class);
                startActivity(introduce);
                return true;

            default:

                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.v("","onResume");
        displayListView();
    }

    private void displayListView() {

        Cursor cursor = dbProductAdapter.fetchAllProducts();


        //Columns to be added
        String[] columns = new String[] {
                DataBaseHelper.PRODUCT_KEY_ROWID,
                DataBaseHelper.PRODUCT_KEY_NAME,
                DataBaseHelper.PRODUCT_KEY_PRICE,
                DataBaseHelper.PRODUCT_KEY_DESCRIPTION,
                DataBaseHelper.PRODUCT_KEY_NAME
        };

        //XML Layout Fields by name
        int[] to = new int[] {
                R.id.code,
                R.id.nameProduct,
                R.id.priceProduct,
                R.id.time,
                R.id.addCartButton
        };

        //Create cursor saving positions and information
        dataAdapter = new SimpleCursorAdapter(this, R.layout.product_info, cursor, columns, to, 0);
        ListView listView = (ListView) findViewById(R.id.listView1);
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                // Get data from the list position
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                // Get name from list position
                String productCode = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.PRODUCT_KEY_NAME));
                Toast.makeText(getApplicationContext(),
                        productCode, Toast.LENGTH_SHORT).show();

                //Calling to Delete Product
                String productID = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.PRODUCT_KEY_ROWID));
                showDialogDelete(productID, productCode);


            }
        });

       EditText myFilter = (EditText) findViewById(R.id.myFilter);

        //Add Textfield to filter listener
        myFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                dataAdapter.getFilter().filter(s.toString());
            }
        });

        //Set Filter function
        dataAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                return dbProductAdapter.fetchProductsByName(constraint.toString());
            }
        });

    }


    private void showDialogDelete(final String productID, final String productName)
    {
        new AlertDialog.Builder(ListProductActivity.this)
                .setTitle(getString(R.string.delete_product, productName))
                .setMessage(delete)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        if(dbProductAdapter.deleteProductID(productID)) {
                            Toast.makeText(ListProductActivity.this, getString(R.string.product_delete_ok, productName), Toast.LENGTH_SHORT).show();
                            displayListView();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public void setProduct(View product)
    {

        //Inside Tag we are stored the ProductID
        PurchaseArray.add(product.getTag().toString());
        Toast.makeText(this, "Product: " + product.getTag().toString(), Toast.LENGTH_SHORT).show();


    }

}