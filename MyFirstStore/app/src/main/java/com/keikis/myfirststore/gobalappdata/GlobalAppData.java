/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore.gobalappdata;
 
import android.app.Application;
 
public class GlobalAppData extends Application{
     
    private String userName;
    private String userRole;


    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRole() {
        return userRole;
    }
     
 
    public String getUserName() {
         
        return userName;
    }
     
    public void setUserName(String userName) {

        this.userName = userName;
         
    }

}