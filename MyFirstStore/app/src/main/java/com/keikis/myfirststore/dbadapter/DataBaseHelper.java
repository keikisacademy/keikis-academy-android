/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore.dbadapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DataBaseHelper extends SQLiteOpenHelper {

    //DB Create
    public static final String DATABASE_NAME = "Shop";
    public static final int DATABASE_VERSION = 1;

    //DB Create Product
    public static final String SQLITE_TABLE_PRODUCT = "Product";
    public static final String PRODUCT_KEY_ROWID = "_id";
    public static final String PRODUCT_KEY_NAME = "name";
    public static final String PRODUCT_KEY_PRICE = "price";
    public static final String PRODUCT_KEY_DESCRIPTION = "description";

    //DB Create User
    public static final String SQLITE_TABLE_USER = "User";
    public static final String USER_KEY_ROWID = "_id";
    public static final String USER_KEY_NAME = "name";
    public static final String USER_KEY_PASS = "pass";
    public static final String USER_KEY_ROLE = "role";
    public static final String USER_KEY_DESCRIPTION = "description";


    //DB Create Purchase
    public static final String SQLITE_TABLE_PURCHASE = "Purchases";
    public static final String PURCHASE_KEY_ROWID = "_id";
    public static final String PURCHASE_KEY_USER = "nameUser";
    public static final String PURCHASE_KEY_PRODUCT = "nameProduct";
    public static final String PURCHASE_KEY_TIME = "time";

    //Create Sentences Tables
    private static final String PRODUCT_CREATE_TABLE_SENTENCE =
            "CREATE TABLE if not exists " + SQLITE_TABLE_PRODUCT + " (" +
                    PRODUCT_KEY_ROWID + " integer PRIMARY KEY autoincrement," +
                    PRODUCT_KEY_NAME + "," +
                    PRODUCT_KEY_PRICE + "," +
                    PRODUCT_KEY_DESCRIPTION + "," +
                    " UNIQUE (" + PRODUCT_KEY_ROWID + "));";

    private static final String USER_CREATE_TABLE_SENTENCE =
            "CREATE TABLE if not exists " + SQLITE_TABLE_USER + " (" +
                    USER_KEY_ROWID + " integer PRIMARY KEY autoincrement," +
                    USER_KEY_NAME + "," +
                    USER_KEY_PASS + "," +
                    USER_KEY_ROLE + "," +
                    USER_KEY_DESCRIPTION + "," +
                    " UNIQUE (" + USER_KEY_NAME + "));";

    private static final String PURCHASE_CREATE_TABLE_SENTENCE =
            "CREATE TABLE if not exists " + SQLITE_TABLE_PURCHASE + " (" +
                    PURCHASE_KEY_ROWID + " integer PRIMARY KEY autoincrement," +
                    PURCHASE_KEY_USER + "," +
                    PURCHASE_KEY_PRODUCT + "," +
                    PURCHASE_KEY_TIME + " date DEFAULT (DATETIME('now','localtime'))," +
                    " FOREIGN KEY(" + PURCHASE_KEY_USER + ") REFERENCES User (" + USER_KEY_NAME + ")" + " ON DELETE CASCADE," +
                    " FOREIGN KEY(" + PURCHASE_KEY_PRODUCT + ") REFERENCES Product (" + PRODUCT_KEY_NAME + " ) ON DELETE CASCADE" +
                    ");";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(USER_CREATE_TABLE_SENTENCE);
        db.execSQL(PRODUCT_CREATE_TABLE_SENTENCE);
        db.execSQL(PURCHASE_CREATE_TABLE_SENTENCE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}