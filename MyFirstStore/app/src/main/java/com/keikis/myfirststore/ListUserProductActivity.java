/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.keikis.myfirstapp.R;
import com.keikis.myfirststore.customcursoradapter.ProductCustomCursorAdapter;
import com.keikis.myfirststore.dbadapter.DataBaseHelper;
import com.keikis.myfirststore.dbadapter.ProductDBAdapter;

public class ListUserProductActivity extends AppCompatActivity {

    static String TAG = "ListUserProductActivity";


    private SimpleCursorAdapter dataAdapter;
    ListView listView;


    private ProductDBAdapter dbProductAdapter;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_user_product);

        dbProductAdapter = new ProductDBAdapter(this);
        dbProductAdapter.open();

        //Clean data
        dbProductAdapter.deleteAllProducts();
        //Add products
        dbProductAdapter.insertSomeProducts();
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.v("","onResume");
        displayListView();


    }

    private void displayListView() {

        Cursor cursor = dbProductAdapter.fetchAllProducts();

        //Columns to be added
        String[] columns = new String[] {
                DataBaseHelper.PRODUCT_KEY_NAME,
                DataBaseHelper.PRODUCT_KEY_PRICE,
                DataBaseHelper.PRODUCT_KEY_DESCRIPTION


        };

        //XML Layout Fields by name
        int[] to = new int[] {
                R.id.nameProduct,
                R.id.priceProduct,
                R.id.time

        };

        //Create cursor saving positions and information
        //dataAdapter = new SimpleCursorAdapter(this, R.layout.product_info_user, cursor, columns, to, 0);
        dataAdapter = new ProductCustomCursorAdapter(this, R.layout.product_info_user, cursor, columns, to, 0);


        listView = (ListView) findViewById(R.id.listView1);



        listView.setAdapter(dataAdapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                // Get cursor data from the list position
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                // Get name from cursor data from the list position
                String productCode = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.PRODUCT_KEY_NAME));
                Toast.makeText(getApplicationContext(), productCode, Toast.LENGTH_SHORT).show();
            }
        });

        EditText myFilter = (EditText) findViewById(R.id.myFilter);

        //Add a listener for the filter
        myFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                dataAdapter.getFilter().filter(s.toString());
            }
        });

        //Set the filter function
        dataAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            public Cursor runQuery(CharSequence constraint) {
                return dbProductAdapter.fetchProductsByName(constraint.toString());
            }
        });

    }



    public void showCart(View product) {

        Intent intent = new Intent(ListUserProductActivity.this, ListPurchaseActivity.class);
        startActivity(intent);

    }

}
