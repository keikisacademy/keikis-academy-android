/*
 * Keikis Academy (c) by Keikis
 *
 * Keikis Academy  is licensed under a
 * Creative Commons Attribution 4.0 Unported License.
 *
 * You should have received a copy of the license along with this
 * work.  If not, see <https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode>.
 */

package com.keikis.myfirststore;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.keikis.myfirstapp.R;
import com.keikis.myfirststore.dbadapter.DataBaseHelper;
import com.keikis.myfirststore.dbadapter.UserDBAdapter;

public class ListUserActivity extends AppCompatActivity {

    private UserDBAdapter dbUserAdapter;
    private SimpleCursorAdapter dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_user);

        dbUserAdapter = new UserDBAdapter(this);
        dbUserAdapter.open();
        displayListView();
    }

    private void displayListView() {

        Cursor cursor = dbUserAdapter.fetchAllUsers();
        //Log.v("Cursor Object", DatabaseUtils.dumpCursorToString(cursor));

        // columnas a las que vincular
        String[] columns = new String[]{
                DataBaseHelper.PURCHASE_KEY_ROWID,
                DataBaseHelper.USER_KEY_NAME,
                DataBaseHelper.USER_KEY_PASS,
                DataBaseHelper.USER_KEY_ROLE,
                DataBaseHelper.USER_KEY_DESCRIPTION
        };

        // vinculamos el xml
        int[] to = new int[]{
                R.id.Id,
                R.id.nameProduct,
                R.id.pass,
                R.id.role,
                R.id.time
        };

        // creamos el cursor guardando las posiciones y su información
        dataAdapter = new SimpleCursorAdapter(this, R.layout.user_info, cursor, columns, to, 0);
        ListView listView = (ListView) findViewById(R.id.listView1);
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                // Obtiene los datos de la posición de la lista
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                // Obtiene  el nombre de la lista de la posición obtenida.
                String productCode = cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.USER_KEY_NAME));
                Toast.makeText(getApplicationContext(),
                        productCode, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
